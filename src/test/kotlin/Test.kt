import org.junit.jupiter.api.Assertions.assertThrows
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class Test {

    @Test fun `When the given node is at the end take exception`() {
        val node = Node(23)
        val exc = assertThrows(IllegalArgumentException::class.java) { deleteMiddleNode(node) }
        assertEquals("Node cannot be a tail.", exc.message)
    }

    @Test fun `When the given node is not at the end take no exception and verify the result`() {
        val head = createLinkedList(23, 34, 56)
        val node: Node = head?.next ?: fail("Internal consistency error")
        val expected: Node = node.next ?: fail("Found an unexpected empty node!")
        deleteMiddleNode(node)
        assertEquals(expected.value, node.value)
        assertEquals(expected.next, node.next)
    }
}
