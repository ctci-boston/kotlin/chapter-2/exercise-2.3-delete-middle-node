data class Node(var value: Int, var next: Node? = null)

fun createLinkedList(vararg values: Int): Node? {
    fun appendToList(head: Node) {
        var current = head
        fun addNodeToList(node: Node) {
            current.next = node
            current = node
        }

        for (n in 1 until values.size) addNodeToList(Node(values[n]))
    }
    val head = if (values.isNotEmpty()) Node(values[0]) else null

    if (head != null && values.size > 1) appendToList(head)
    return head
}

fun deleteMiddleNode(node: Node) {
    fun copyNext(source: Node) {
        node.value = source.value
        node.next = source.next
    }
    val next = node.next
    requireNotNull(next) { "Node cannot be a tail." }
    copyNext(next)
}
